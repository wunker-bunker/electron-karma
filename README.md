[![pipeline status](https://gitlab.com/wunker-bunker/electron-karma/badges/main/pipeline.svg)](https://gitlab.com/wunker-bunker/electron-karma/-/commits/main)

# Electron Karma

This docker image includes all the dependencies needed to run karma with an Electron browser.
