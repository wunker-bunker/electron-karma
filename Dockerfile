FROM electronuserland/builder
RUN apt-get update && apt-get install -y libatk1.0-0 libasound2 libatk-bridge2.0-0 librust-gdk-pixbuf-sys-dev libgtk-3-0 xvfb libnss3
ENV DISPLAY=:99
